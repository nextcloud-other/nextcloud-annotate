<?php
declare(strict_types=1);
namespace OCA\PDFAnnotate;

use OCP\Capabilities\IPublicCapability;

class Capabilities implements IPublicCapability {

	public function getCapabilities() {
		return [
			'pdfannotate' => [
				'enabled' => true,
			],
		];
	}

}
