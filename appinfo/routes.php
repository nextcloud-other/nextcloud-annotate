<?php
declare(strict_types=1);
namespace OCA\PDFAnnotate\AppInfo;

return [
	'routes' => [
		[
			'name' => 'viewer#index',
			'url' => '/{fileId}',
			'verb' => 'GET',
		],
		[
			'name' => 'viewer#viewer',
			'url' => '/viewer/{fileId}',
			'verb' => 'GET',
		],
		[
			'name' => 'viewer#downloadFile',
			'url' => '/d/{fileId}',
			'verb' => 'GET',
		],
		[
			'name' => 'api#saveFile',
			'url' => '/save/{fileId}',
			'verb' => 'POST',
		],
                [
                        'name' => 'manipulator#addPage',
                        'url' => '/addpage/{fileId}',
                        'verb' => 'POST',
		],
		[
			'name' => 'manipulator#manipulator',
			'url' => '/manipulator/{fileId}',
			'verb' => 'GET',
		],
		[
			'name' => 'manipulator#preview',
			'url' => '/p/{fileId}/{page}',
			'verb' => 'GET',
		],
		[
			'name' => 'manipulator#save',
			'url' => '/manipulator/save',
			'verb' => 'POST',
		],
		[
			'name' => 'manipulator#newProject',
			'url' => '/manipulator/new/{fileId}/{fileName}',
			'verb' => 'GET',
		],
	]
];
