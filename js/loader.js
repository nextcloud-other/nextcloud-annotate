(function(OCA) {
  OCA.PDFAnnotate = OCA.PDFAnnotate || {};

  /**
   * @namespace OCA.FilesPdfViewer.PreviewPlugin
   */
  OCA.PDFAnnotate.PreviewPlugin = {

    /**
     * @param fileList
     */
    attach: function(fileList) {
      if (fileList.$el && fileList.$el.attr('id') === 'app-content-trashbin') {
        // Don't add action to files in trashbin.
        return;
      }

      fileList.fileActions.registerAction({
        displayName: t('pdfannotate', 'Annotate'),
        iconClass: 'icon-edit',
        name: 'Annotate',
        mime: 'application/pdf',
        permissions: OC.PERMISSION_READ,
        actionHandler: function(fileName, context) {
          var fileInfoModel = context.fileInfoModel || context.fileList.getModelForFile(fileName);
          this.show(fileInfoModel.id);
        }.bind(this)
      });

      fileList.fileActions.setDefault('application/pdf', 'Annotate');

      fileList.fileActions.registerAction({
        displayName: t('pdfannotate', 'Manipulate'),
        iconClass: 'icon-projects',
        name: 'Manipulate',
        mime: 'application/pdfmanip',
        permissions: OC.PERMISSION_READ,
        actionHandler: function(fileName, context) {
          var fileInfoModel = context.fileInfoModel || context.fileList.getModelForFile(fileName);
          this.showManip(fileInfoModel.id);
        }.bind(this)
      });

      fileList.fileActions.setDefault('application/pdfmanip', 'Manipulate');
    },

    show: function(id) {
      var url = OC.generateUrl('/apps/pdfannotate/{id}', {
        id: id
      });

      location.href = url;
    },

    showManip: function(id) {
      var url = OC.generateUrl('/apps/pdfannotate/manipulator/{id}', {
        id: id
      });

      location.href = url;
    }
  };

  OCA.PDFAnnotate.AddPDFManip = {
    attach: function (menu) {
        menu.addMenuEntry({
            id: 'pdfman',
            displayName: 'New PDF manipulation',
            templateName: 'new Manipulation',
            iconClass: 'icon-projects',
            fileType: 'file',
            actionHandler: function (name) {
                // https://download.bitgrid.net/nextcloud/jsdocs/OCA.Files.FileList.html
                console.log('do something here:', name, menu.fileList);
		var directoryId = menu.fileList.dirInfo.id;
		var url = OC.generateUrl('/apps/pdfannotate/manipulator/new/{id}/{name}', {
                  id: directoryId,
                  name: name
                });
		console.log('URL:', url);
		console.log('directory fileId:', menu.fileList.dirInfo.id);
		var xhr = new XMLHttpRequest();
                xhr.open('GET', url, true);
                xhr.onreadystatechange = function(){
                  if (this.readyState === XMLHttpRequest.DONE && this.status === 200){
                    console.log("Page added to manipulation project");
                    setTimeout(function() {
                      menu.fileList.reload();
                    }, 1000);
                    //location.reload();
                  } else if (this.readyState === XMLHttpRequest.DONE){
                    console.log("ERROR adding page");
                    alert("Unable to add new PDF manipulation project. Please ensure that you have write permission in this directory, and that there is not already a manipulation project present.");
                  }
                }
                xhr.send();
            }
        });
    }
  };

})(OCA);

OC.Plugins.register('OCA.Files.FileList', OCA.PDFAnnotate.PreviewPlugin);
OC.Plugins.register('OCA.Files.NewFileMenu', OCA.PDFAnnotate.AddPDFManip);

var oldShow;
if (OCA.FilesPdfViewer && OCA.FilesPdfViewer.PreviewPlugin) {
  oldShow = OCA.FilesPdfViewer.PreviewPlugin.show;

  OCA.FilesPdfViewer.PreviewPlugin.show = function(url, params, isFileList) {
    if (typeof(isFileList) === "undefined") {
      // Nextcloud 14 doesn't pass the "params".
      isFileList = params;
    }
    if (!isFileList) {
      // Prevent opening the PDF when user clicks on publicly shared file.
      return;
    }

    return oldShow.apply(this, arguments);
  };
}
